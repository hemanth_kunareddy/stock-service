/**
 * 
 */
package com.telstra.homepage.stockservice.service;

import com.telstra.homepage.stockservice.model.StockUpdates;

/**
 * @author Hemanth.Kunareddy
 *
 */
public interface StockUpdatesService {

	public StockUpdates getStockUpdates(String zipCode);
}
