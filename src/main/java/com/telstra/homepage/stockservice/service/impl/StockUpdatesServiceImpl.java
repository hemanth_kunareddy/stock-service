/**
 * 
 */
package com.telstra.homepage.stockservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telstra.homepage.stockservice.dao.StockUpdatesDAO;
import com.telstra.homepage.stockservice.model.StockUpdates;
import com.telstra.homepage.stockservice.service.StockUpdatesService;

/**
 * @author Hemanth.Kunareddy
 *
 */
@Service
public class StockUpdatesServiceImpl implements StockUpdatesService {
	
	@Autowired
	StockUpdatesDAO stockServiceDAO;

	@Override
	public StockUpdates getStockUpdates(String zipCode) {
		// TODO Auto-generated method stub
		StockUpdates stockUpdates = stockServiceDAO.getStockUpdates(zipCode);
		return stockUpdates;
	}

}
