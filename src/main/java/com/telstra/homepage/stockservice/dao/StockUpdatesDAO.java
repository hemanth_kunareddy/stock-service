package com.telstra.homepage.stockservice.dao;

import org.springframework.stereotype.Component;

import com.telstra.homepage.stockservice.model.StockUpdates;





@Component
public class StockUpdatesDAO {

	public StockUpdates getStockUpdates(String zipCode) {
		StockUpdates stockUpdates= new StockUpdates();
		
		if(zipCode.equalsIgnoreCase("500050")) {
			 stockUpdates.setStockName("Google");
			 stockUpdates.setStockPrice("20000");
			 return stockUpdates;
	      }
		else if (zipCode.equalsIgnoreCase("500032")) {
			stockUpdates.setStockName("Infosys");
			 stockUpdates.setStockPrice("15000");
			 return stockUpdates;
		}
		
		return stockUpdates;
	}
}
