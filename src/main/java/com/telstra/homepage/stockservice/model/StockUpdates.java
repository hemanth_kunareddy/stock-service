/**
 * 
 */
package com.telstra.homepage.stockservice.model;

/**
 * @author Hemanth.Kunareddy
 *
 */
public class StockUpdates {

	private String StockName;
	private String StockPrice;
	
	public String getStockName() {
		return StockName;
	}
	public void setStockName(String stockName) {
		StockName = stockName;
	}
	public String getStockPrice() {
		return StockPrice;
	}
	public void setStockPrice(String stockPrice) {
		StockPrice = stockPrice;
	}
	
	
	
}
