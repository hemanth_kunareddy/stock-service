/**
 * 
 */
package com.telstra.homepage.stockservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @author Hemanth.Kunareddy
 *
 */
@SpringBootApplication
public class StockServiceMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(StockServiceMain.class, args);
	}

}
