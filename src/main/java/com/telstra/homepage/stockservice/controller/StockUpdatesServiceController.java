/**
 * 
 */
package com.telstra.homepage.stockservice.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.homepage.stockservice.model.StockUpdates;
import com.telstra.homepage.stockservice.service.StockUpdatesService;


/**
 * @author Hemanth.Kunareddy
 *
 */
@RestController
@RequestMapping(value ="/api/v1")
public class StockUpdatesServiceController {

	@Autowired
	StockUpdatesService stockUpdatesService;
	
	@GetMapping("/stock-updates/{zipCode}")
	public ResponseEntity<StockUpdates> getStockUpdates(@PathVariable("zipCode") String zipCode){
	    StockUpdates stockUpdates =  stockUpdatesService.getStockUpdates(zipCode);  	
		return new ResponseEntity<> (stockUpdates, HttpStatus.OK); 
	}
}
